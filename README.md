## MashaDB 

A Python API for MySQL and MariaDB

## Requirements

+ [Python](https://python.org/)
+ [pipenv](https://pipenv.pypa.io/en/latest/)
+ [mysql-connector-python](https://pypi.org/project/mysql-connector-python/)

## Basic Usage

### Connect and Commit:

     db = MashaDB(**config)
     db.connect(database=db_name)
     db.commit()
     db.closeall()

### Write Operations:

    db.table.write(column=data, column=data, column=data)
    db.table.write(**data)

### Read Operations:

    data = db.table.select(column, column).all(sort=column)
    data = db.table.select(column).where(clause, limit=10)

### With Context Manager:

    Automatically commits data and closes the connection.

    from functools import partial
    masha = partial(MashaDB, **config)
    
    with masha(database=database) as db:
        db.table.write(**data)
        query = db.table.select(column).all()

### Create a Table:

    db.create('table', **kwargs)
    db.create('table', column='datatype', column='datatype')
    db.create('users', id='INT AUTO_INCREMENT PRIMARY KEY')

    import Columns, Primary

    primary = Primary()

    name = Column('VARCHAR(100)')
    email = Column('VARCHAR(255', unique=True)
                
    db.create(id=primary.key, Name=name.column, Email=email.column)

## Issues

+ currently testing in production

## Reference

+ [MariaDB](https://mariadb.com)
+ [MySQL](https://www.mysql.com)

## License

+ [Apache](http://opensource.org/licenses/Apache-2.0)
